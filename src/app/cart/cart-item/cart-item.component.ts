import { Component, OnInit ,Input} from '@angular/core';

import {CartItem,Cart} from '../cart';

@Component({
  selector: 'cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

  constructor() { }

  @Input() item: CartItem;

  ngOnInit() {


  }

  delete(item: CartItem){

  }

}
