import {Product} from '../product/product';

export class Cart{
  products: CartItem[] = [];
}

export class CartItem implements Serializeable<CartItem>{
  product: Product;
  name: String;
  price: number;
  tax_free_price: number;
  id: number;
  amount: number;
  tstamp: number;
  deserialize(json): CartItem{
    this.id = json.id;
    this.tstamp = json.tstamp;
    this.name = json.name;
    this.amount = json.quantity;
    this.price = json.price;
    this.tax_free_price = json.tax_free_price;
    this.product = new Product().deserialize(json.product);

    return this;
  }
}

export interface Serializeable<T>{
  deserialize(input: Object): T;
}
