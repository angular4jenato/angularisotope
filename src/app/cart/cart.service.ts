import { Injectable } from '@angular/core';

import {Product} from '../product/product';
import {Http,RequestOptions,Headers} from '@angular/http';

import {Observable} from 'rxjs/Observable';
import {Cart,CartItem} from './cart';

@Injectable()

export class CartService {

  root = 'http://isotope.jsdutywiz.com/api/';
  headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : 'Basic cGlsbGU6cHIwMTExanQ='});
  options = new RequestOptions({ headers: this.headers});
  constructor(private http: Http) { }

  addToCart(id: number,amount: number): Observable<Cart>{

     let cart:Cart = new Cart();

     return this.http.post(this.root,JSON.stringify({"action": "addToCart","productId": id,"quantity": amount}),{withCredentials: true})
       .map(response => {
         console.log(response, response)
         if(response.json().length == 0){

         }
         else{
           response.json().items.forEach(cartItem =>{
             cart.products.push(new CartItem().deserialize(cartItem));
           });
         }
        console.log(cart);
         return cart;
       });
  }

  getCart(): Observable<Cart>{
    let cart:Cart = new Cart();

    return this.http.post(this.root, JSON.stringify({"action": "getCart"}), {withCredentials: true} )
      .map(response => {
        // console.log(response.json());
        if(response.json().length == 0){

        }
        else{
          response.json().items.forEach(cartItem =>{
            cart.products.push(new CartItem().deserialize(cartItem));
          });
        }
        return cart;
      });
  }

}
