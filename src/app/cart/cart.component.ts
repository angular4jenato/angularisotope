import { Component, OnInit } from '@angular/core';

import {Cart,CartItem} from './cart';

import {Login} from '../login/login';

import {Router} from '@angular/router';

import {CartService} from './cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(private router: Router,private cartService: CartService) { }

  cart: Cart = new Cart();

  ngOnInit() {
    this.cartService.getCart()
      .subscribe(cart => {this.cart = cart;console.log(this.cart)});

      // console.log(this.cart);
  }

  getPrice(): number{
    let s = 0;
    this.cart.products.forEach(item =>{
      s += item.price;
    });
    return s;
  }

  getMwst(): number{
    let s = 0;
    this.cart.products.forEach(item =>{
      s += item.price - item.tax_free_price;
    });

    return s;
  }

  getTaxFreePrice(): number{
    let s = 0;
    this.cart.products.forEach(item =>{
      s += item.tax_free_price;
    });

    return s;
  }

  buy(cart: Cart){
    if(Login.getInstance().loggedIn){
      this.router.navigate(['buy']);
    }
    else{
      this.router.navigate(['/login']);
    }
  }

}
