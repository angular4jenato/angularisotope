import { Component, OnInit,Input } from '@angular/core';


import {Product} from '../../product/product';
import {Cart,CartItem} from '../cart';

import {CartService} from '../cart.service';
@Component({
  selector: 'cart-button',
  templateUrl: './cart-button.component.html',
  styleUrls: ['./cart-button.component.scss']
})
export class CartButtonComponent implements OnInit {

  constructor(private cartService:CartService) { }
  @Input() product: Product
  cart: Cart = new Cart();
  ngOnInit() {

  }

  addToCart(product: Product,amount: number): void{

    // this.cartService.addToCart(product.id,amount);
      this.cartService.addToCart(product.id,amount).subscribe();


}
}
