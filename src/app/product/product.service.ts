import { Injectable } from '@angular/core';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

import {Http,RequestOptions,Headers} from '@angular/http';
import {Product} from './product';

@Injectable()
export class ProductService {

  constructor(private http:Http) { }


  getProducts(): Observable<Product[]>{

    var root = 'http://isotope.jsdutywiz.com/api/';
    var products: Product[] = [];
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : 'Basic cGlsbGU6cHIwMTExanQ='});
    let options = new RequestOptions({ headers: headers});
    return this.http.post(root, JSON.stringify({action: "productlist"}))
      .map(response => {

        response.json().forEach(product =>{

        products.push(new Product().deserialize(product));
          }
          );


        return products;
      })
  }

  getProduct(alias: String): Observable<Product>{
    return this.getProducts()
      .map(products => products.find(product => product.alias === alias));
  }
}
