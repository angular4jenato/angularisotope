export class Price implements Serializeable<Price>{
  price: number;
  currency: String;
  currencyFormat: String;
  priceRoundPrecision: number;

  deserialize(json): Price{
    var price = json.price;
    this.price = price.price;
    this.currency = price.currency;
    this.currencyFormat = price.currencyFormat;
    this.priceRoundPrecision = price.priceRoundPrecision;
    return this;
  }
}

export class Image implements Serializeable<Image>{
  src: String;
  singleSrc: String;

  deserialize(json): Image{
    this.src = json.src;
    this.singleSrc = json.singleSRC;
    return this;
  }

}

export class Product implements Serializeable<Product>{
  id: number;
  timestamp: Date;
  language: String;
  dateAdded: Date;
  alias: String;
  name: String;
  description: String;
  sku: number;
  images: Image[] = [];
  sorting: number;
  price: Price;

  deserialize(json): Product{
    this.id = json.id;
    this.timestamp = new Date(json.tstamp * 1000);
    this.language = json.language;
    this.dateAdded = new Date(json.dateAdded * 1000);
    this.alias = json.alias;
    this.name = json.name;
    this.description = json.description;
    this.sku = json.sku;
    this.images = [];
    json.images.forEach(image =>
      this.images.push(new Image().deserialize(image))
    );
    this.sorting = json.sorting;
    this.price = new Price().deserialize(json);

    return this;
  }
}

export interface Serializeable<T>{
  deserialize(input: Object): T;
}
