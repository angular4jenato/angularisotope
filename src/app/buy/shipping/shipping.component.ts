import { Component, OnInit } from '@angular/core';
import {BuyService} from '../buy.service';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.css']
})
export class ShippingComponent implements OnInit {

  constructor(private buyService: BuyService) { }

  ngOnInit() {
    this.buyService.orderStatus = 1;
  }

}
