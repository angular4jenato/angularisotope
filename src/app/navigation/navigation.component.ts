import { Component, OnInit,Input } from '@angular/core';

import {Cart} from '../cart/cart';

import {CartService} from '../cart/cart.service';
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private cartService: CartService) { }

  @Input() page: String;

  ngOnInit() {
  
  }

}
