import { Injectable } from '@angular/core';

import {Http,Headers,RequestOptions} from '@angular/http';

import {Member} from './login';

import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {

  constructor(private http:Http) { }


  auth(user: String,password: String): Observable<any>{
    var root = 'http://isotope.jsdutywiz.com/api/';
    var headers = new Headers();
    headers.append("Authorization", "Basic " + "cGhpbGlwcDphdWZzdGllZyMxMg==");

    var body = JSON.stringify({action : "getMember"});

    return this.http.post(root,body,{headers: headers})
      .map(response => {
        if(response.json().error){
          console.log(response.json().error);
          return response.json().error;
        }
        else{
          return new Member().deserialize(response.json());
        }
      })
  }
}
