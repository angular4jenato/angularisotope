import {Serializeable} from '../product/product';

export class Login{
  private static instance: Login;
  loggedIn: boolean;
  member: Member;
  static getInstance(){
    if(!Login.instance){
      Login.instance = new Login();
      Login.instance.loggedIn = false;
      Login.instance.member = new Member();
    }
    return Login.instance;
  }
}

export class Member implements Serializeable<Member>{
  firstName: String;
  lastName : String;
  userName: String;

  deserialize(json): Member{
    this.firstName = json.firstName;
    this.lastName = json.lastName;
    this.userName = json.userName;

    return this;
  }

}
