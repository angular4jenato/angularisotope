import { Component, OnInit } from '@angular/core';

import {Product} from '../product/product';

import {ProductService} from '../product/product.service';

@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.css']
})
export class ProductOverviewComponent implements OnInit {

  constructor(private productService: ProductService) { }
  products: Product[];
  ngOnInit() {
    this.productService.getProducts()
      .subscribe(products => this.products = products);
  }

}
