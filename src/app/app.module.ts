import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';

import { AppComponent } from './app.component';
import { StartpageComponent } from './startpage/startpage.component';

import {AppRoutingModule} from './app-routing/app-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductSmallComponent } from './product-list/product-small/product-small.component';

import {ProductService} from './product/product.service';
import {LoginService} from './login/login.service';
import {CartService} from './cart/cart.service';
import {BuyService} from './buy/buy.service';

import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductBigComponent } from './product-list/product-big/product-big.component';
import { LoginComponent } from './login/login.component';
import { CartComponent } from './cart/cart.component';
import { CartButtonComponent } from './cart/cart-button/cart-button.component';
import { ErrorComponent } from './error/error.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { ProductOverviewComponent } from './product-overview/product-overview.component';
import { BuyComponent } from './buy/buy.component';
import { ShippingComponent } from './buy/shipping/shipping.component';
import { BillingComponent } from './buy/billing/billing.component';
import { CheckoutComponent } from './buy/checkout/checkout.component';


@NgModule({
  declarations: [
    AppComponent,
    StartpageComponent,
    ProductListComponent,
    ProductSmallComponent,
    ProductDetailComponent,
    ProductBigComponent,
    LoginComponent,
    CartComponent,
    CartButtonComponent,
    ErrorComponent,
    NavigationComponent,
    CartItemComponent,
    ProductOverviewComponent,
    BuyComponent,
    ShippingComponent,
    BillingComponent,
    CheckoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [ProductService,ProductSmallComponent,LoginService,CartService,BuyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
