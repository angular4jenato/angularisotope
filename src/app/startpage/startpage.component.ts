import { Component, OnInit } from '@angular/core';

import {ProductSmallComponent} from '../product-list/product-small/product-small.component';
import {ProductService} from '../product/product.service';

import {Product} from '../product/product';
@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.component.html',
  styleUrls: ['./startpage.component.scss']
})
export class StartpageComponent implements OnInit {

  pageTitle: String = 'Webshop';
  products: Product[] = [];
  firstProduct: Product;



  constructor(private productService: ProductService,private smallComp: ProductSmallComponent) { }



  ngOnInit() {

    this.getFirstProduct();
    
  }


  getFirstProduct(){

    this.productService.getProducts()
      .subscribe(products =>
        this.firstProduct = products[0]
        //console.log(products[0]);
        //console.log(this.firstProduct);
      );


  }


}
