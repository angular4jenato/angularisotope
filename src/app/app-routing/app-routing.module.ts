import { NgModule } from '@angular/core';

import {Routes, RouterModule} from '@angular/router';

import {StartpageComponent} from '../startpage/startpage.component';
import {ProductDetailComponent} from '../product-detail/product-detail.component';
import {LoginComponent} from '../login/login.component';
import {CartComponent} from '../cart/cart.component';
import {ProductOverviewComponent} from '../product-overview/product-overview.component';
import {BuyComponent} from '../buy/buy.component';
import {BillingComponent} from '../buy/billing/billing.component';
import {ShippingComponent} from '../buy/shipping/shipping.component';
import {CheckoutComponent} from '../buy/checkout/checkout.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch:'full'},
  {path: 'home', component: StartpageComponent},
  {path: 'product/:alias', component: ProductDetailComponent},
  {path: 'login', component: LoginComponent},
  {path: 'warenkorb', component: CartComponent},
  {path: 'produkte', component: ProductOverviewComponent},
  {path: 'kaufen', component: BuyComponent,
    children: [
      {path: '', redirectTo: 'versand', pathMatch:'full'},
      {path: 'versand',component: ShippingComponent},
      {path: 'rechnung',component: BillingComponent},
      {path: 'checkout',component: CheckoutComponent}

    ]
  }

];

@NgModule({

  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
