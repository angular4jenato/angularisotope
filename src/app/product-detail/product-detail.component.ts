import { Component, OnInit,Input } from '@angular/core';

import {Product} from '../product/product';

import {ProductService} from '../product/product.service';

import { ActivatedRoute, ParamMap ,Router} from '@angular/router';

import 'rxjs/add/operator/switchMap';

import {Login} from '../login/login';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  constructor(private productService:ProductService,private route:ActivatedRoute,private router: Router) { }

  product: Product;
  login: any;

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => this.productService.getProduct(params.get('alias')))
      .subscribe(product => this.product = product);
      this.login = Login.getInstance();

  }

  gotoLogin(){
    this.router.navigate(['/login']);
  }

}
