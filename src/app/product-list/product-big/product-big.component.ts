import { Component, OnInit ,Input} from '@angular/core';

import {Product} from '../../product/product';
@Component({
  selector: 'product-big',
  templateUrl: './product-big.component.html',
  styleUrls: ['./product-big.component.scss']
})
export class ProductBigComponent implements OnInit {

  constructor() { }
  @Input() product: Product;

  ngOnInit() {

    
  }

}
