import { Component, OnInit } from '@angular/core';

import {ProductService} from '../product/product.service';

import {Product} from '../product/product';
@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  constructor(private productService: ProductService) { }
  products: Product[] = [];

  ngOnInit() {

    this.productService.getProducts()
      .subscribe(products => this.products = products.slice(0,3));

  }

}
