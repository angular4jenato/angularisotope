import { Component, OnInit ,Input} from '@angular/core';

import {Router} from '@angular/router';

import {Product} from '../../product/product';

import {ProductBigComponent} from '../product-big/product-big.component';
@Component({
  selector: 'product-small',
  templateUrl: './product-small.component.html',
  styleUrls: ['./product-small.component.scss']
})
export class ProductSmallComponent implements OnInit {

  constructor(private router: Router) { }

  @Input() product: Product;
  src: String;
  ngOnInit() {
    this.src = 'http://isotope.jsdutywiz.com/' + this.product.images[0].singleSrc;
  }

  detailView(product: Product){

    this.router.navigate(['/product',product.alias]);

  }

}
